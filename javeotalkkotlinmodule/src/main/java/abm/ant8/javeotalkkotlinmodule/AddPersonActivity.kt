package abm.ant8.javeotalkkotlinmodule

import abm.ant8.javeotalkkotlinmodule.R.style.ThemeOverlay_AppCompat_Dark_ActionBar
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PEOPLE_TABLE_NAME
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PERSON_NAME_COLUMN
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PERSON_SURNAME_COLUMN
import abm.ant8.javeotalkkotlinmodule.dbUtils.database
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout

class AddPersonActivity : AppCompatActivity() {
    lateinit var toolbar: Toolbar

    companion object {
        val FRESHLY_INSERTED = "FRESHLY_INSERTED"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val textStyling = { v: View ->
            when (v) {
                is EditText  -> v.textSize = 16f
                is TextView -> v.textSize = 16f
            }
        }

        coordinatorLayout {
            fitsSystemWindows = true
            appBarLayout(ThemeOverlay_AppCompat_Dark_ActionBar) {
                toolbar = toolbar {
                    setTitleTextColor(Color.WHITE)
                }.lparams(width = matchParent, height = matchParent)

            }.lparams(width = matchParent)

            linearLayout {
                orientation = LinearLayout.VERTICAL
                padding = dip(16)
                textView("name") {
                }.lparams(width = matchParent)
                val nameEdit = editText {
                    id = R.id.name_edit
                }.lparams(width = matchParent)
                textView("surname") {
                }.lparams(width = matchParent)
                val surnameEdit = editText {
                    id = R.id.surname_edit
                }.lparams(width = matchParent)
                button("add person") {
                    onClick {
                        saveDataAndExit(nameEdit.text.toString(), surnameEdit.text.toString())
                    }
                    backgroundColor = Color.TRANSPARENT
                }.lparams(width = matchParent)
            }.lparams(width = matchParent, height = matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }.applyRecursively(textStyling)
        }

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun saveDataAndExit(name: String, surname: String) {
        doAsync {
            var freshlyInserted: Long = 0
            database.use {
                freshlyInserted = insert(PEOPLE_TABLE_NAME,
                        PERSON_NAME_COLUMN to name,
                        PERSON_SURNAME_COLUMN to surname)
                freshlyInserted
            }
            database.close()

            uiThread {
                setResult(Activity.RESULT_OK, Intent()
                        .putExtra(FRESHLY_INSERTED, freshlyInserted))
                finish() }
        }
    }
}

