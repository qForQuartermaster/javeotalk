package abm.ant8.javeotalkkotlinmodule

import android.graphics.Color
import android.support.design.widget.AppBarLayout.ScrollingViewBehavior
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainUI(val activity: MainActivity) : AnkoComponent<MainActivity> {

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        coordinatorLayout {
            fitsSystemWindows = true

            appBarLayout {
                toolbar {
                    setTitleTextColor(Color.WHITE)
                    id = R.id.toolbar
                }.lparams(width = matchParent, height = matchParent)

            }.lparams(width = matchParent)

            relativeLayout {
                id = R.id.container
                recyclerView {
                    id = R.id.recycler_view
                    adapter = activity.adapter
                    layoutManager = LinearLayoutManager(ctx)
                }.lparams(width = matchParent, height = matchParent)
            }.lparams(width = matchParent, height = matchParent) {
                behavior = ScrollingViewBehavior()
            }
            floatingActionButton {
                onClick { activity.startAddPersonActivity() }
                imageResource = R.drawable.ic_add_white_24dp
            }.lparams {
                gravity = Gravity.BOTTOM or Gravity.END
                margin = dip(16)
            }
        }
    }
}

