package abm.ant8.javeotalkkotlinmodule

import abm.ant8.javeotalkkotlinmodule.model.Person
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*

class MainUIAdapter(val people: MutableList<Person>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun addPerson(person: Person?) {
        if (person != null) {
            people.add(person)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return PersonViewHolder(PeopleUI(viewType).createView(AnkoContext.create(parent!!.context, parent)))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder!! as PersonViewHolder).bind(people[position])
    }

    override fun getItemCount() = people.size

    fun getViewTypeCount() = 2

    override fun getItemViewType(position: Int) = position % getViewTypeCount()

}

class PeopleUI(var viewType: Int) : AnkoComponent<ViewGroup> {
    private val styling = { v: View ->
        when (v) {
            is TextView -> { v.textSize = 16f
                             v.width = matchParent }
        }
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                orientation = LinearLayout.VERTICAL
                when (viewType) {
                   1 -> backgroundColor = R.color.colorAccent
                }
                lparams(width = matchParent, height = dip(48))
                textView {
                    id = R.id.name_text_view
                }
                textView {
                    id = R.id.surname_text_view
                }
            }.applyRecursively { styling }
        }
    }
}

class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val name = itemView.find<TextView>(R.id.name_text_view)
    val surname = itemView.find<TextView>(R.id.surname_text_view)

    fun bind(person: Person) {
        name.text = person.name
        surname.text = person.surname
    }
}