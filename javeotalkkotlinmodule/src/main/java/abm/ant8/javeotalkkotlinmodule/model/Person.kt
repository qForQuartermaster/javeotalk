package abm.ant8.javeotalkkotlinmodule.model

data class Person (val name: String, val surname: String)