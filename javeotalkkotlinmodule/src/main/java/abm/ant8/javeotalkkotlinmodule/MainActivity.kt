package abm.ant8.javeotalkkotlinmodule

import abm.ant8.javeotalkkotlinmodule.AddPersonActivity.Companion.FRESHLY_INSERTED
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PEOPLE_TABLE_NAME
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PERSON_NAME_COLUMN
import abm.ant8.javeotalkkotlinmodule.dbUtils.DBOpenHelper.Companion.PERSON_SURNAME_COLUMN
import abm.ant8.javeotalkkotlinmodule.dbUtils.database
import abm.ant8.javeotalkkotlinmodule.model.Person
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.parseOpt
import org.jetbrains.anko.db.select
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {
    val toolbar by lazy {
        find<Toolbar>(R.id.toolbar)
    }
    val people = mutableListOf<Person>()
    val adapter by lazy {
        MainUIAdapter(people)
    }

    val GET_PERSON = 1234

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainUI(this).setContentView(this)

        setSupportActionBar(toolbar)

        getPeopleFromDB()
    }

    fun startAddPersonActivity() {
        startActivityForResult(Intent(this, AddPersonActivity::class.java), GET_PERSON)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GET_PERSON)
            if (resultCode == Activity.RESULT_OK) {
                getNewPerson(data)
            }
    }

    private fun getPeopleFromDB() {
        var peopleFromDb = listOf<Person>()
        doAsync {
            database.use {
                peopleFromDb = select(PEOPLE_TABLE_NAME)
                        .column(PERSON_NAME_COLUMN)
                        .column(PERSON_SURNAME_COLUMN)
                        .exec { parseList(classParser<Person>()) }

            }
            database.close()

            uiThread {
                people.clear()
                peopleFromDb.forEach {
                    adapter.addPerson(it)
                }
            }
        }
    }

    private fun getNewPerson(data: Intent?) {
        var person: Person? = null

        doAsync {
            database.use {
                person = select(PEOPLE_TABLE_NAME)
                        .column(PERSON_NAME_COLUMN)
                        .column(PERSON_SURNAME_COLUMN)
                        .whereSimple("_id = ?", data?.getLongExtra(FRESHLY_INSERTED, -1).toString())
                        .exec { parseOpt(classParser<Person>()) }
            }
            database.close()
            uiThread {
                adapter.addPerson(person)
            }
        }
    }
}