package abm.ant8.javeotalkkotlinmodule.dbUtils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DBOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyDatabase", null, 1) {
    companion object {
        private var instance: DBOpenHelper? = null

        val PEOPLE_TABLE_NAME = "People"
        val PERSON_NAME_COLUMN = "name"
        val PERSON_SURNAME_COLUMN = "surname"

        @Synchronized
        fun getInstance(ctx: Context): DBOpenHelper {
            if (instance == null) {
                instance = DBOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db?.createTable(PEOPLE_TABLE_NAME, true,
                "_id" to INTEGER + PRIMARY_KEY + UNIQUE,
                    PERSON_NAME_COLUMN to TEXT,
                    PERSON_SURNAME_COLUMN to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db?.dropTable("People", true)
    }
}

val Context.database: DBOpenHelper
    get() = DBOpenHelper.getInstance(applicationContext)