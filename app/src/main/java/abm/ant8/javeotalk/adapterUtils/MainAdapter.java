package abm.ant8.javeotalk.adapterUtils;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import abm.ant8.javeotalk.R;

public class MainAdapter extends RecyclerView.Adapter {

	List<Person> people;

	public MainAdapter() {
		this.people = new LinkedList<>();
	}

	public void addPerson(Person person) {
		if (people == null)
			people = new LinkedList<>();
		Log.d("TAG", "dodałem człowieka " + person.getName());
		people.add(person);
		notifyDataSetChanged();
	}
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == 0) {
			return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.person_list_item, parent, false));
		} else {
			return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.person_list_item_even, parent, false));
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		Log.d("TAG", "pokazuję ludka " + people.get(position).getName());
		((ViewHolder) holder).name.setText(people.get(position).getName());
		((ViewHolder) holder).surname.setText(people.get(position).getSurname());
	}

	@Override
	public int getItemCount() {
		return people.size();
	}

	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		return position % getViewTypeCount();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public TextView name;
		public TextView surname;

		public ViewHolder(View itemView) {
			super(itemView);
			name = (TextView) itemView.findViewById(R.id.name);
			surname = (TextView) itemView.findViewById(R.id.surname);
		}
	}}
