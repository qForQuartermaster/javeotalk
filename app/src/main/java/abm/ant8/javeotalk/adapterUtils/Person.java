package abm.ant8.javeotalk.adapterUtils;

public class Person {
	private final String name;

	private final String surname;

	public Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getSurname() {
		return surname;
	}

	public String getName() {
		return name;
	}
}
