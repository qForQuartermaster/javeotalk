package abm.ant8.javeotalk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddPersonActivity extends AppCompatActivity {
	EditText name;
	EditText surname;
	Button addPersonButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_add_person);
		name = (EditText) findViewById(R.id.name);
		surname = (EditText) findViewById(R.id.surname);
		addPersonButton = (Button) findViewById(R.id.add_person_button);
		addPersonButton.setOnClickListener(new AddPersonOnClickListener());
	}

	private class AddPersonOnClickListener implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			Intent returnIntent = new Intent();
			returnIntent.putExtra(MainActivity.NAME, name.getText().toString());
			returnIntent.putExtra(MainActivity.SURNAME, surname.getText().toString());
			setResult(RESULT_OK, returnIntent);
			finish();
		}
	}
}

